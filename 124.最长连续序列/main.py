class Solution:
    """
    @param num: A list of integers
    @return: An integer
    """
    def longestConsecutive(self, num):
        # write your code here
        num = set(num)
        max = 1
        for x in num:
            if x - 1 not in num:
                right = x + 1
                while right in num:
                    right += 1
                
                if max < right - x:
                    max = right - x
        
        return max