#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2020年09月05日
版    本：v1.0.0
描    述：
Copyright (C) 2020 All rights reserved.
'''


class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number and the index of the last number
    """
    def subarraySum(self, nums):
        # write your code here
        posmap = dict()
        
        numsum = 0
        posmap[0] = -1

        for i in range(len(nums)):
            numsum += nums[i]
            pos = posmap.get(numsum, None)
            if pos is not None:
                return [pos + 1, i]
            
            posmap[numsum] = i
 
        return []


if __name__ == "__main__":
    a = Solution()
    print a.subarraySum([1,0,1])