给定一个整数数组，找到一个和最接近于零的子数组。返回第一个和最右一个指数。你的代码应该返回满足要求的子数组的起始位置和结束位置

数据保证任意数的和都在[−231,231−1][-2^{31},2^{31}-1][−2​31​​,2​31​​−1]范围内
您在真实的面试中是否遇到过这个题？  
样例

样例1

输入: 
[-3,1,1,-3,5] 
输出: 
[0,2]
解释: [0,2], [1,3], [1,1], [2,2], [0,4]

挑战

O(nlogn)的时间复杂度