import sys


class Solution:
    """
    @param: nums: A list of integers
    @return: A list of integers includes the index of the first number and the index of the last number
    """
    def subarraySumClosest(self, nums):
        # write your code here
        size = len(nums)
        if size <= 1:
            return [0, 0]

        prefix_sum = [[0, -1]]
        for i in range(0, size):
            prefix_sum.append([prefix_sum[i][0] + nums[i], i])

        prefix_sum.sort(key=lambda x: x[0])

        close = sys.maxsize
        for i in range(1, len(prefix_sum)):
            if close > prefix_sum[i][0] - prefix_sum[i-1][0]:
                close = prefix_sum[i][0] - prefix_sum[i-1][0]
                left = min(prefix_sum[i][1], prefix_sum[i-1][1]) + 1
                right = max(prefix_sum[i][1], prefix_sum[i-1][1])
                ans = [left, right]

        return ans


if __name__ == "__main__":
    a = Solution()
    print a.subarraySumClosest([3,-3,5])