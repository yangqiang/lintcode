/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月19日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition of singly-linked-list:
 *
 * class ListNode {
 * public:
 *     int val;
 *     ListNode *next;
 *     ListNode(int val) {
 *        this->val = val;
 *        this->next = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param head: n
     * @return: The new head of reversed linked list.
     */
    ListNode * reverse(ListNode * head) {
        // write your code here
        if( !head )
            return NULL;

        ListNode pre = ListNode(-1);
        pre.next = head;
        ListNode *p = head->next;
        while( p ){
            head->next = p->next;
            p->next = pre.next;
            pre.next = p;
            p = head->next;
        }

        return pre.next;
    }
};

