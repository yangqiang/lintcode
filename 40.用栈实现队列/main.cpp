class MyQueue {
public:
    stack<int> s1;
    stack<int> s2;
    MyQueue() {
        // do intialization if necessary
    }

    /*
     * @param element: An integer
     * @return: nothing
     */
    void push(int element) {
        // write your code here
        s1.push(element);
    }

    /*
     * @return: An integer
     */
    int pop() {
        if( s2.empty() ){
            while( !s1.empty() ){
                s2.push(s1.top());
                s1.pop();
            }
        }
        int tmp = s2.top();
        s2.pop();
        return tmp;
        // write your code here
    }

    /*
     * @return: An integer
     */
    int top() {
        // write your code here
        if( s2.empty() ){
            while( !s1.empty() ){
                s2.push(s1.top());
                s1.pop();
            }
        }
        return s2.top();
    }
};