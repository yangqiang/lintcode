class Solution {
public:
    /**
     * @param nums: A list of integers
     * @return: A integer indicate the sum of max subarray
     */
    int maxSubArray(vector<int> &nums) {
        // write your code here
		int max = INT_MIN;
        int len = nums.size();
        int i = 0, sum = 0;
        while( i < len ){
            sum += nums[i++];
            if( sum > max ){
                max = sum;
            }
            if( sum < 0 ){
                sum = 0;
            }
        }

        return max;
    }
};

