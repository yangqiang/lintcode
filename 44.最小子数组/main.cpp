class Solution {
public:
    /*
     * @param nums: a list of integers
     * @return: A integer indicate the sum of minimum subarray
     */
    int minSubArray(vector<int> &nums) {
        // write your code here
        int len = nums.size();
        if( len == 0 )
            return 0;
        int min = INT_MAX, sum = 0, i = 0;
        while( i < len ){
            sum += nums[i++];
            if( sum < min )
                min = sum;
            if( sum > 0 )
                sum = 0;
        }

        return min;
	}
};

