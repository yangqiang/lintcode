/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月22日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    /*
     * @param chars: The letter array you should sort by Case
     * @return: nothing
     */
    void sortLetters(string &chars) {
        // write your code here
        int len = chars.size();
        if( len <= 1 )
            return;

        int left = 0, right = len - 1;
        while( left < right ){
            while( left < right && islower(chars[left]) )
                left++;
            while( left < right && isupper(chars[right]) )
                right--;

            swap(chars[left], chars[right]);
        }

        return;
    }
};
