/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月22日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <string>
using namespace std;

class Solution {
public:
    /*
     * @param s: A string
     * @return: A string
     */

    void reverse(string &s, int start, int end){
        int len = s.size();
        if( len <= 1 )
            return;

        int left = start, right = end;
        while( left < right ){
            swap(s[left], s[right]);
            left++;
            right--;
        }

        return;
    }

    string reverseWords(string &s) {
        // write your code here
        int len = s.size();
        if( len <= 1 )
            return s;

        reverse(s, 0, end - 1);
        
        int i = 0, j = 0;
        while( i < len ){
            while( isspace(s[i]) )
                i++;
            j = i + 1;
            while( j < len && !isspace(s[j]) )
                j++;

            reverse(s, i, j - 1);
            i = j;
        }
        
        return s;
    }
};
