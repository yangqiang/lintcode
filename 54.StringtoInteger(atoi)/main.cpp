/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月22日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <string>

using namespace std;

class Solution {
public:
    /**
     * @param str: A string
     * @return: An integer
     */
    int atoi(string &str) {
        // write your code here
        int len = str.size();
        int flag = 1;
        int i = 0, result = 0;
        while( isspace(str[i]) )
            i++;

        if( i < len && str[i] == '-' ){
            flag = -1;
            i++;
        }else if( i < len && str[i] == '+' ){
            i++;
        }
        
        for(; i < len; i++){
            if( isdigit(str[i]) ){
                if( result > (INT_MAX - (str[i] - '0'))/10 )
                    return flag > 0 ? INT_MAX : INT_MIN;

                result = result * 10 + str[i] - '0';
            } else{
                result = 0;
                break;
            }
        }

        result *= flag;
        return result;
    }
};
