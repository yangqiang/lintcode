/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月26日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
#include <map>

using namespace std;

class Solution {
public:
    /**
     * @param numbers: An array of Integer
     * @param target: target = numbers[index1] + numbers[index2]
     * @return: [index1, index2] (index1 < index2)
     */
    vector<int> twoSum(vector<int> &numbers, int target) {
        // write your code here
        int len = numbers.size();
        map<int, int> hmap;
        vector<int> ret(2, -1);
        for( int i = 0; i < len; i++ )
            hmap.insert(pair<int, int>( numbers[i], i ));

        int j = 0, tmp=0;;
        for(int k = 0; k < len; k++){
            tmp = target - numbers[k];
            if( hmap.count(tmp) ){
                j = hmap[tmp];
                if( j < k ){
                    ret[0] = j;
                    ret[1] = k;
                    return ret;
                }
            }
        }
        
        return ret;
    }
};
