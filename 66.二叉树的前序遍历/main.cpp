/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param root: A Tree
     * @return: Preorder in ArrayList which contains node values.
     */
    vector<int> preorderTraversal(TreeNode * root) {
        // write your code here
        stack<TreeNode *> s;
        vector<int> result;
        if( !root )
            return result;
        
        s.push(root);
        TreeNode *p = NULL;
        while( !s.empty() ){
            p = s.top();
            result.push_back(p->val);
            s.pop();
  
            if( p->right )
                s.push(p->right);
            if( p->left )
                s.push(p->left);
        }

        return result;
    }
};
