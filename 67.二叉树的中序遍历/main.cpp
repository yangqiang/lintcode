/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月15日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>

using namespace std;

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param root: A Tree
     * @return: Inorder in ArrayList which contains node values.
     */
    vector<int> inorderTraversal(TreeNode * root) {
        // write your code here
        vector<int> result;
        if( !root ){
            return result;
        }
        
        stack<TreeNode *> s;
        TreeNode *p = root;
        while( p || !s.empty() ){
            if( p ){
                s.push(p);
                p = p->left;
            }else{
                p = s.top();
                result.push_back(p->val);
                s.pop();
                p = p->right;
            }
        }

        return result;
	}
};
