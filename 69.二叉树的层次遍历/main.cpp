/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年07月15日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <vector>
#include <stack>
#include <queue>

using namespace std;
/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param root: A Tree
     * @return: Level order a list of lists of integer
     */
    vector<vector<int>> levelOrder(TreeNode * root) {
        // write your code here
        vector<vector<int> > result;
        vector<int> tmp;
        if( !root ){
            return result;
        }
        queue<TreeNode *> q;
        TreeNode *cur = NULL;
        q.push(root);
        int len = q.size();
        while( !q.empty() ){
            len = q.size();
            while( len-- ){
                cur = q.front();
                tmp.push_back(cur->val);
                q.pop();
                if( cur->left )
                    q.push(cur->left);
                if( cur->right )
                    q.push(cur->right);
            }
            result.push_back(tmp);
            tmp.clear();
        }
	}
};
