/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年09月02日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>

using namespace std;

class Solution {
public:
    /**
     * @param A: An integers array.
     * @return: return any of peek positions.
     */
    int findPeak(vector<int> &nums) {
        // write your code here
        int low = 0, high = nums.size() - 1;
        if( high == 0 )
            return 0;
        int mid1 = 0, mid2 = 0;
        while( low < high ){
            mid1 = low + (high - low)/2;
            mid2 = mid1 + 1;    
            if( nums[mid1] < nums[mid2] )
                low = mid2;
            else
                high = mid1;
        }
        
        return low;
    }
};
