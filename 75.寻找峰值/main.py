#!/usr/bin/env pytho3
#-*- coding:utf-8 -*-

'''
文件名称：main.py
创 建 者：yang qiang
创建日期：2018年09月02日
描    述：
'''

class Solution:
    """
    @param A: An integers array.
    @return: return any of peek positions.
    """
    def findPeak(self, A):
        # write your code here
        left = 0
        right = len(A) - 1
        mid1, mid2 = 0, 0
        while left < right:
            mid1 = (right - left)//2 + left
            mid2 = mid1 + 1
            if A[mid1] >= A[mid2]:
                right = mid1
            else:
                left = mid2

        return right

