给k个字符串，求出他们的最长公共前缀(LCP)
样例

样例 1:
	输入:  "ABCD", "ABEF", "ACEF"
	输出:  "A"
	

样例 2:
	输入: "ABCDEFG", "ABCEFG" and "ABCEFA"
	输出:  "ABC"

