/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年03月13日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Solution {
public:
    /**
     * @param strs: A list of strings
     * @return: The longest common prefix
     */
    string longestCommonPrefix(vector<string> &strs) {
        int size = strs.size();
        string prefix = "";
        if(size == 0){
            return prefix;
        }

        for(int i = 0; i < strs[0].length(); i++){
            for(int j = 1; j < size; j++){
                if(strs[j].length() == i || (strs[j][i] != strs[0][i])){
                    return prefix;
                }

            }
            
            prefix += strs[0][i];
        }

        return prefix;
    }
};