/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月12日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    /**
     * @param A: An integer array
     * @return: An integer
     */
    int singleNumber(vector<int> &A) {
        // write your code here
        int result = 0;
        int len = A.size();
        for(int i = 0; i < len; i++){
            result ^= A[i];
        }

        return result;
    }
};
