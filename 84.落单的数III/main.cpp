/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月12日
*   描    述：
*   
================================================================*/


#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    /*
     * @param A: An integer array
     * @return: An integer array
     */
    vector<int> singleNumberIII(vector<int> &nums) {
        // write your code here
        int len = nums.size();
        vector<int> result(2, 0);
        if( len <= 1 )
            return result;
        
        int res = 0;
        for(int i = 0; i < len; i++){
            res ^= nums[i];
        }

        int flag = res & ~(res - 1);
        for(int j = 0; j < len; j++){
            if( nums[j] & flag )
                result[0] ^= nums[j];
        }

        result[1] = result[0] ^ res;

        return result;
    }
};
