/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2020年08月10日
*   版    本：v1.0.0
*   描    述：
*   Copyright (C) 2020 All rights reserved.
*   
* ================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */


class Solution {
public:
    /*
     * @param root: The root of the binary search tree.
     * @param node: insert this node into the binary search tree
     * @return: The root of the new binary search tree.
     */
    TreeNode * insertNode(TreeNode * root, TreeNode * node) {
        // write your code here
        if(root == NULL){
            return node;
        }

        if(node->val > root->val){
            root->right = insertNode(root->right, node);
        }else{
            root->left = insertNode(root->left, node);
        }

        return root;
    }
};


class Solution {
public:
    /*
     * @param root: The root of the binary search tree.
     * @param node: insert this node into the binary search tree
     * @return: The root of the new binary search tree.
     */
    TreeNode * insertNode(TreeNode * root, TreeNode * node) {
        if(root == NULL){
            return node;
        }

        TreeNode* curroot = root;
        while(curroot != node){
            if(node->val > curroot->val){
                if(curroot->right == NULL){
                    curroot->right = node;
                }

                curroot = curroot->right;
            }else{
                if(curroot->left == NULL){
                    curroot->left = node;
                }

                curroot = curroot->left;
            }
        }

        return root;
    }
};