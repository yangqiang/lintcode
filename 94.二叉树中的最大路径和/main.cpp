/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年09月09日
*   描    述：
*   
================================================================*/


#include <iostream>

using namespace std;

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param root: The root of binary tree.
     * @return: An integer
     */
    int maxPathSum(TreeNode * root) {
        // write your code here
        int ret = INT_MIN;
        onepath(root, &ret);
        return ret;
    }

    int onepath(TreeNode *root, int *ret){
        if( !root )
            return 0;

        int l = onepath(root->left, ret);
        int r = ontpath(root->right, ret);
        *ret = max(*ret, max(0, l) + max(0, r)+root->value);
        return max(0, max(l, r)) + root->value;
    }

};
