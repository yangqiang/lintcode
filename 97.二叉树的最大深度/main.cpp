/*================================================================
*   文件名称：main.cpp
*   创 建 者：yang qiang
*   创建日期：2018年08月16日
*   描    述：
*   Copyright (C) 2018 All rights reserved.
*   
================================================================*/


#include <iostream>
#include <queue>
using namespace std;

/**
 * Definition of TreeNode:
 * class TreeNode {
 * public:
 *     int val;
 *     TreeNode *left, *right;
 *     TreeNode(int val) {
 *         this->val = val;
 *         this->left = this->right = NULL;
 *     }
 * }
 */

class Solution {
public:
    /**
     * @param root: The root of binary tree.
     * @return: An integer
     */
    int maxDepth(TreeNode * root) {
        // write your code here
        if( !root )
            return 0;

        queue<TreeNode *> q;
        q.push(root);
        int maxdepth = 0;
        int size = 0;
        TreeNode *p = NULL;

        while( !q.empty() ){
            size = q.size();
            while( size-- ){
                p = q.front();
                q.pop();
                if( p->left )
                    q.push(p->left);
                if( p->right )
                    q.push(p->right);
            }

            maxdepth++;
        }

        return maxdepth;
	}
};

